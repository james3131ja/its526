﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore
{
    class Book
    {
    private string isbn,title;
    private double price;
    public string ISBN { get { return isbn; } set { this.isbn=value;} }
    public string Title { get { return title; } set { this.title=value;} }
    public double Price { get { return price; } set { this.price=value;} }
    public Book() {
        ISBN = "TBD";
        Title = "TBD";
        Price = 0;
    }
    public Book(string isbn,string title,double price) {
        ISBN = isbn;
        Title = title;
        Price = price;
    }

    public override string ToString()
    {
        return "ISBN: " +isbn + "\nTitle: "+ title + "\nPrice: " +price;
    }

    }
}
