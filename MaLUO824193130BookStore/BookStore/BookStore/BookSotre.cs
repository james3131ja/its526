﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStore
{
    class BookSotre
    {   
        private SortedList<int,Book> bookStore = null;

        public SortedList<int, Book> BookStore { get { return bookStore; } set { bookStore = value; } }
        
        public BookSotre() {
            bookStore = new SortedList<int, Book>();
        }
        public BookSotre(SortedList<int, Book> list)
        {
            bookStore = list;
        }

        public void AddBook(Book b) {
            try
            {
                bookStore.Add(Convert.ToInt32(b.ISBN), b);
            }
            catch (Exception e) { }
        }

        public void RemoveBook(Book b) {
            try
            {
                bookStore.Remove(Convert.ToInt32(b.ISBN));
            }
            catch (Exception e) { }
        }

        public Book FindBook(string tmp) {
            Book b = new Book();
            bool check = false;
            try
            {
                foreach (KeyValuePair<int, Book> i in bookStore)
                {
                    if (i.Key == Convert.ToInt32(tmp))
                    {
                        check = true;
                        return i.Value;       
                    }
                }
            }
            catch (Exception e) { }
            return b;
        }
        
        public void Dislay(ListView lv) { 
            foreach(KeyValuePair<int,Book> item in this.bookStore){
                ListViewItem row = new ListViewItem(item.Key.ToString());
                row.SubItems.Add(item.Value.Title);
                row.SubItems.Add(Convert.ToString(item.Value.Price));
                lv.Items.Add(row);
            }
        }
    }
}
