﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStore
{
    public partial class Form1 : Form
    {

        private static BookSotre bStore = new BookSotre();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (MyValidate.Validate(txtISBN) && MyValidate.Validate(txtTitle)
                && MyValidate.Validate(txtPrice) && MyValidate.isNumeric(txtISBN)
                && MyValidate.isNumeric(txtPrice))
            {
                MessageBox.Show("Validation Passed");
                Book b = new Book(txtISBN.Text, txtTitle.Text, Convert.ToDouble(txtPrice.Text));
                bStore.AddBook(b);
                iniListView();
                cleanText();
            }
        }

      

        private void Form1_Load(object sender, EventArgs e)
        {
            iniListView();
        }
        

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {

            if (MyValidate.Validate(txtTargetBook) && MyValidate.isNumeric(txtTargetBook))
            {
                MessageBox.Show("Validation Passed");
                Book b = new Book();
                b.ISBN = txtTargetBook.Text;
                bStore.RemoveBook(b);
                iniListView();
                cleanText();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
             string tmp ="";
            if (MyValidate.Validate(txtTargetBook) && MyValidate.isNumeric(txtTargetBook))
            {
                MessageBox.Show("Validation Passed");
                tmp = txtTargetBook.Text;
                Book book = bStore.FindBook(tmp);
                iniListView();
                MessageBox.Show(book.ToString());
                cleanText();
            }
        }

        private void iniListView()
        {
            lvBooks.Clear();
            lvBooks.Columns.Add("ISBN");
            lvBooks.Columns.Add("Title");
            lvBooks.Columns.Add("Price");
            bStore.Dislay(lvBooks);
        }

        private void cleanText()
        {
            txtISBN.Text = "";
            txtTitle.Text = "";
            txtPrice.Text = "";
            txtTargetBook.Text = "";
        }
    }
}
