﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStore
{
    class MyValidate
    {
        public static bool Validate(TextBox text) {
            bool b = false;
            if (text.Text == "")
            {
                MessageBox.Show("Fill " + text.Name + " Please!");
            }
            else {
                b = true;
            }
            return b;
        }

        public static bool isNumeric(TextBox text)
        {
            bool b = false;
            Regex regex = new Regex(@"^\d+$");
            Match match = regex.Match(text.Text);

            if (!match.Success)
            {
                MessageBox.Show(text.Text + " not numeric");
            }
            else {
  
                b = true;
            }
            return b;
        }
    }
}
