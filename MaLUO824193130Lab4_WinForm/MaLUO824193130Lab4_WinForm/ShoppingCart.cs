﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaLUO824193130Lab4_WinForm
{
    public class ShoppingCart
    {
        //common part
        private LinkedList<ItemOrder> orderList = null;
        private double total = 0.0;
        
        public LinkedList<ItemOrder> OrderList { 
            get { return orderList; }
            set { orderList=value;} 
        }
        public double Total { get { return total; } set { total=value;} }

        public ShoppingCart() { 
        //empty at first
        orderList = new LinkedList<ItemOrder>();
        total = this.countTotal();
        }
        public double countTotal(){
            double total = 0.0;
            ItemOrder tmpOrder = null;
            foreach (ItemOrder itemOrder in this.orderList) {
                tmpOrder = itemOrder;
                switch(discountOption(tmpOrder)){
                    case 0:
                    total += tmpOrder.Item.Price*0.8 * tmpOrder.Quantity;
                    break;
                    case 1:
                    total += tmpOrder.Item.Price * 0.8 * (tmpOrder.Quantity-1) + tmpOrder.Item.Price;
                    break;
                    case 2: 
                    total += tmpOrder.Item.Price;
                    break;
                }
            }
            return total;
        }
        public int discountOption(ItemOrder itemOrder) {
            return itemOrder.Quantity % 2;
        }

        public void printOrdersWithNumber() {
            int count = 0;
            Console.WriteLine("..........Shopping Cart..........");
            foreach(ItemOrder itemOrder in this.orderList){
                count++;
                Console.WriteLine(count+". "+itemOrder.Item.Name +"  "+itemOrder.Quantity);            
            }
            Console.WriteLine("The toral price is : " +countTotal());
            Console.WriteLine("..........Shopping Cart..........");
        }
    
        //winform part

        //add an item to order list and display it in the order listview
        public void addItem(ListView lvItems,  ListView lvOrders, TextBox txtItemNum, TextBox txtQuantity, Label lblTotalAmount)
        {
            bool b = false;
            //check if the item num is in the item list
            foreach (ListViewItem i in lvItems.Items)
            {
                //MessageBox.Show(Convert.ToString(i.Text));
                if (Convert.ToString(i.Text) == Convert.ToString(txtItemNum.Text)
                    && Convert.ToInt32(txtQuantity.Text) > 0) { b = true; }
            }
            //if item number is in the item list
            if (b == true)
            {
                LinkedList<Item> itemList = iniItem();
                /*add an item
                use get item to get the specific num of item in the linkedlist*/
                Item item = new Item(getItem(itemList, Convert.ToInt32(txtItemNum.Text)).Name, getItem(itemList, Convert.ToInt32(txtItemNum.Text)).Price);
                ItemOrder itemOrder = new ItemOrder(item, Convert.ToInt32(txtQuantity.Text));
                //already defined in shopping cart class
                orderList.AddLast(itemOrder);

                displayOrderList(lvOrders);
                MessageBox.Show("Item Ordered.  Please check shopping cart");
            }
            
            else
            {
                MessageBox.Show("Item Not Exsisting Or Order Not validated");
            }
            //reset the textboxes
            txtItemNum.Text = "";
            txtQuantity.Text = "";
        }
       
        public void removeItems(ListView lvOrders)
        {
            String check = "";
            LinkedList<ItemOrder> tmpList = null;
            ListView.CheckedListViewItemCollection checkedItems = lvOrders.CheckedItems;
            foreach (ListViewItem item in checkedItems)
            {
                lvOrders.Items.Remove(item);
                //get the order number
                check = Convert.ToString(item.SubItems[1].Text);
                tmpList = CloneList(orderList);
                foreach (ItemOrder itemOrder in tmpList)
                {
                    if (itemOrder.Item.Name.Equals(check))
                    {
                        orderList.Remove(itemOrder);
                    }
                }
            }
            MessageBox.Show("Orders Canceled");
        }

        public LinkedList<ItemOrder> CloneList(LinkedList<ItemOrder> o)
        {
            LinkedList<ItemOrder> tmpList = new LinkedList<ItemOrder>();
            foreach (ItemOrder item in o)
            {
                //tmpList.AddAfter();
                tmpList.AddLast(item);
            }
            return tmpList;
        }
        public  LinkedList<Item> iniItem()
        {
            LinkedList<Item> itemList = new LinkedList<Item>();
            itemList.AddLast(new Item("Computer", 25));
            itemList.AddLast(new Item("Laptop", 50));
            return itemList;
        }
        public  Item getItem(LinkedList<Item> itemList, int i)
        {
            Item tmp = null;
            int count = 0;
            foreach (Item item in itemList)
            {
                if (count == i) { tmp = item; }
                count++;
            }
            return tmp;
        }
        public void displayItemList(ListView lvItems, LinkedList<Item> itemList)
        {
            int count = 0;
            foreach (Item i in itemList)
            {
                //first colum in the item is 1 now
                ListViewItem row = new ListViewItem(Convert.ToString(count));
                row.SubItems.Add(i.Name);
                row.SubItems.Add(Convert.ToString(i.Price));
                lvItems.Items.Add(row);
                count++;
            }
            lvItems.Columns.Add("Item Number", 80);
            lvItems.Columns.Add("Item Name", 80);
            lvItems.Columns.Add("Price", 80);
        } 
        public void displayOrderList(ListView lvOrders) {
            //display items in order listview
            //clearn all the text first
            lvOrders.Clear();
            lvOrders.Columns.Add("Order Number", 80);
            lvOrders.Columns.Add("Item Name", 80);
            lvOrders.Columns.Add("Quantity", 80);
            int count = 0;
            foreach (ItemOrder i in orderList)
            {
                ListViewItem row = new ListViewItem(Convert.ToString(count));
                row.SubItems.Add(i.Item.Name);
                row.SubItems.Add(Convert.ToString(i.Quantity));
                lvOrders.Items.Add(row);
                count++;
            }
        }
    }
}
