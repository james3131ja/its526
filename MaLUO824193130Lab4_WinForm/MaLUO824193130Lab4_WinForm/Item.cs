﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaLUO824193130Lab4_WinForm
{
    public class Item
    {
        private string name = null;
        private double price = 0.0;

        public string Name { get { return name; } set { name = value; } }
        public double Price { get { return price; } set { price = value;} }
        
        public Item() { Name = "TBD";}
        public Item(double price) { Name = "TBD"; Price = price; }
        public Item(string name) { Name = name; }
        public Item(string name,double price) {
            Name = name;
            Price = price;
        }
      
    }
}
