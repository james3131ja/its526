﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaLUO824193130Lab4_WinForm
{
    public class ItemOrder
    {
        private Item item = null;
        private int quantity = 0;

        public Item Item { get { return item; } set { item = value;} }
        public int Quantity { get { return quantity; } set { quantity=value;} }
        public ItemOrder() { }
        public ItemOrder(Item item,int quantity) {
            Item = item;
            Quantity = quantity;
        }
    }
}
