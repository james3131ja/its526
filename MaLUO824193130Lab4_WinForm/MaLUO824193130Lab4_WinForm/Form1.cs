﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaLUO824193130Lab4_WinForm
{
    public partial class Form1 : Form
    {

        private ShoppingCart cart = new ShoppingCart();
        
        public Form1()
        {
            InitializeComponent();   
        }

        //initialize item list
        private void Form1_Load(object sender, EventArgs e)
        {
            LinkedList<Item> itemList = cart.iniItem();
            cart.displayItemList(lvItems,itemList);
            tabPage1.Text = @"Order";
            tabPage2.Text = @"Cart";
        }
       

        private void btnOrder_Click(object sender, EventArgs e)
        {
            cart.addItem(lvItems,lvOrders,txtItemNum,txtQuantity,lblTotalAmount);
            lblTotalAmount.Text = Convert.ToString(cart.countTotal()); 
        }

        //remove list
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            cart.removeItems(lvOrders);
            lblTotalAmount.Text = Convert.ToString(cart.countTotal());
        }
    }
}
